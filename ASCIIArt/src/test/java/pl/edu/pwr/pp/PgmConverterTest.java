package pl.edu.pwr.pp;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;

import java.awt.image.BufferedImage;
import org.junit.Before;
import org.junit.Test;

public class PgmConverterTest {

	ImageFileReader reader;
	PgmConventer pgmconv;
	BufferedImage image;

	@Before
	public void setUp() {
		reader = new ImageFileReader();
		pgmconv = new PgmConventer();
	}

	
	@Test
	public void shouldThrowExceptionWhenPathIsWrong() {
		
		String filePath = "/home/ola/Desktop/testImage.pgm";

			image = pgmconv.convert(filePath);
			assertThat(image, is(instanceOf(BufferedImage.class)));
	}
	
	
	@Test
	public void shouldReturnBufferedImageFromURL() {
		
		String fileURL = "http://agata.migalska.staff.iiar.pwr.wroc.pl/PP2016/obrazy/ziemia.pgm";

			image = pgmconv.convertFromUrl(fileURL);
			assertThat(image, is(instanceOf(BufferedImage.class)));
	}
}
