package pl.edu.pwr.pp;

import java.awt.Color;
import java.awt.image.BufferedImage;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

public class GrayConventerTest {

	@Mock private BufferedImage image;
	GrayConventer gconv;
	int rgb; 
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		gconv  = new GrayConventer();
		Color color = new Color(50,100,150);
		rgb = color.getRGB();		
	}

	@Test
	public void shouldReturnRightValueConvertedPixel() {
		
		Mockito.when(image.getWidth()).thenReturn(1);
		Mockito.when(image.getHeight()).thenReturn(1);
		Mockito.when(image.getRGB(0,0)).thenReturn(rgb);

		int[][] result = gconv.convert(image);		
		Assert.assertThat(result[0][0], Matchers.is(Matchers.equalTo(90)));
		
		Mockito.verify(image).getWidth();
		Mockito.verify(image).getHeight();
		Mockito.verify(image).getRGB(0, 0);
	}
}
