package pl.edu.pwr.pp;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class QualityDialog extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4731706638106903158L;
	private final JPanel contentPanel = new JPanel();
	private JTextField valueTextField;

	private Integer quality = 10;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			QualityDialog dialog = new QualityDialog();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public Integer getQuality() {
		return quality;
	}

	/**
	 * Create the dialog.
	 */
	public QualityDialog() {
		valueTextField = new JTextField();
		setBounds(100, 100, 450, 175);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);

		JLabel lblWybierz = new JLabel("Wybierz ilość poziomów szarości");
		lblWybierz.setBounds(10, 11, 212, 14);
		contentPanel.add(lblWybierz);

		JSlider slider = new JSlider();
		slider.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {

				JSlider slider = (JSlider) e.getSource();
				if (!slider.getValueIsAdjusting())
					valueTextField.setText(new Integer(slider.getValue()).toString());

			}
		});
		slider.setValue(10);
		slider.setMinimum(1);
		slider.setMaximum(70);
		slider.setBounds(10, 40, 200, 23);
		contentPanel.add(slider);

		valueTextField.setEditable(false);
		valueTextField.setBounds(214, 40, 49, 23);
		contentPanel.add(valueTextField);
		valueTextField.setColumns(10);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						quality = slider.getValue();
						setVisible(false);
						dispose();
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						setVisible(false);
						dispose();
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
	}
}
