package pl.edu.pwr.pp;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.JLabel;
import javax.swing.JSlider;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class ScaleDialog extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1341183921925076724L;
	private final JPanel contentPanel = new JPanel();
	private JTextField textFieldSliderValue;

	private Integer scale = 100;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			ScaleDialog dialog = new ScaleDialog();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public Integer getScale() {
		return scale;
	}
	/**
	 * Create the dialog.
	 */
	public ScaleDialog() {
		JSlider slider = new JSlider();
		slider.setMaximum(200);
		slider.setMinimum(1);
		slider.setValue(100);
		setBounds(100, 100, 450, 167);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		{
			JLabel lblSkalowanieObrazu = new JLabel("Skalowanie obrazu");
			lblSkalowanieObrazu.setBounds(10, 11, 200, 14);
			contentPanel.add(lblSkalowanieObrazu);
		}
		{
			slider.setBounds(10, 36, 200, 23);
			contentPanel.add(slider);
		}
		{
			textFieldSliderValue = new JTextField();
			textFieldSliderValue.setEditable(false);
			textFieldSliderValue.setBounds(220, 39, 86, 20);
			contentPanel.add(textFieldSliderValue);
			textFieldSliderValue.setColumns(10);
			textFieldSliderValue.setText(scale.toString());
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						scale = slider.getValue();
						setVisible(false);
						dispose();
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						setVisible(false);
						dispose();
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
		
		slider.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {

				JSlider slider = (JSlider) e.getSource();
				if (!slider.getValueIsAdjusting())
					textFieldSliderValue.setText(new Integer(slider.getValue()).toString());

			}
		});
		
	}

}
