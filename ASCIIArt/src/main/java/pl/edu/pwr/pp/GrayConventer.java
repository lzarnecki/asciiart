package pl.edu.pwr.pp;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.util.stream.IntStream;

public class GrayConventer {

	public int[][] convert(BufferedImage img) {
		
		int width = img.getWidth();
		int height = img.getHeight();

		int [][] intensities = new int[width][];

		// strumien
		IntStream.range(0, width).forEach(
			i -> intensities[i] = new int[height]);

		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				Color color = new Color(img.getRGB(x, y));
				intensities[x][y] = (int) (0.2989 * color.getRed() + 0.587 * color.getGreen()
						+ 0.114 * color.getBlue());
			}
		}

		return intensities;
	}
}