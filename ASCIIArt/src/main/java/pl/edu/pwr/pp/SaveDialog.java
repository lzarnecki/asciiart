package pl.edu.pwr.pp;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.event.ActionListener;
import java.io.File;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;

public class SaveDialog extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7803295767220301185L;
	private final JPanel contentPanel = new JPanel();
	private JTextField textFieldFilePath;
	private File file = null;
	private String filePath = null;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			SaveDialog dialog = new SaveDialog();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	String getFilePath() {
		return filePath;
	}

	/**
	 * Create the dialog.
	 */
	public SaveDialog() {

		final JFileChooser fc = new JFileChooser();

		setBounds(100, 100, 350, 188);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		{
			JLabel lblWybierz = new JLabel("Wybierz miejsce do zapisu pliku");
			lblWybierz.setBounds(10, 26, 205, 25);
			contentPanel.add(lblWybierz);
		}
		{
			JButton btnNewButton = new JButton("Save...");
			btnNewButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {

					int retrival = fc.showSaveDialog(null);
					if (retrival == JFileChooser.APPROVE_OPTION) {

						file = fc.getSelectedFile();
						filePath = file.getPath();
						textFieldFilePath.setText(filePath);
					}

				}
			});
			btnNewButton.setBounds(225, 27, 89, 23);
			contentPanel.add(btnNewButton);
		}
		{
			textFieldFilePath = new JTextField();
			textFieldFilePath.setEditable(false);
			textFieldFilePath.setBounds(10, 62, 316, 20);
			contentPanel.add(textFieldFilePath);
			textFieldFilePath.setColumns(10);
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						setVisible(false);
						dispose();
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						filePath = null;
						setVisible(false);
						dispose();
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
	}

}
