package pl.edu.pwr.pp;

import java.io.File;
import java.io.PrintWriter;

public class ImageFileWriter {

	public void saveToTxtFile(char[][] ascii, String fileName) {
		try {
			File file = new File(fileName);

			PrintWriter printWriter = new PrintWriter(file);

			for (int i = 0; i < ascii.length; i++) {
				for (int j = 0; j < ascii[i].length; j++) {
					printWriter.append(ascii[i][j]);
				}
				printWriter.print("\n");
			}

			printWriter.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
