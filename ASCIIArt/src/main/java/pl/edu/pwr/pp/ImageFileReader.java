package pl.edu.pwr.pp;

import java.io.BufferedReader;
import java.io.Console;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;

public class ImageFileReader {

	private int width=0;
	private int height=0;
	
	public int getWidth()
	{
		return width;
	}
	
	public int getHeight()
	{
		return height;
	}
	
	public int [][] readPgmFileURL(String urlPath) throws MalformedURLException
	{
		int columns = 0;
		int rows = 0;
		int[][] intensities = null;
		URL url = null;
		
		url = new URL(urlPath);

        try (BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()))) {
			// w kolejnych liniach kodu można / należy skorzystać z metody
			// reader.readLine()
			
			// pierwsza linijka pliku pgm powinna zawierać P2
			// TODO : Wasz kod
			if(!reader.readLine().equals("P2"))
			{
				throw new IOException("Zły format pliku.");
			}
			
				
			
			// druga linijka pliku pgm powinna zawierać komentarz rozpoczynający
			// się od #
			// TODO : Wasz kod
			
			if(!reader.readLine().startsWith("#"))
			{
				throw new IOException("Zły format pliku.");
			}
			
			// trzecia linijka pliku pgm powinna zawierać dwie liczby - liczbę
			// kolumn i liczbę wierszy (w tej kolejności). Te wartości należy
			// przypisać do zmiennych columns i rows.
			// TODO : Wasz kod
			
			String readLine = reader.readLine();
			String[] splitLine = readLine.split("\\s+");
			
			columns = Integer.parseInt(splitLine[0]);
			rows = Integer.parseInt(splitLine[1]);
			
			width=columns;
			height=rows;
			
			
			// czwarta linijka pliku pgm powinna zawierać 255 - najwyższą
			// wartość odcienia szarości w pliku
			// TODO : Wasz kod
			
			if(!reader.readLine().equals("255"))
			{
				throw new IOException("Zły format pliku.");
			}
			

			// inicjalizacja tablicy na odcienie szarości
			intensities = new int[rows][];

			for (int i = 0; i < rows; i++) {
				intensities[i] = new int[columns];
			}

			// kolejne linijki pliku pgm zawierają odcienie szarości kolejnych
			// pikseli rozdzielone spacjami
			String line = null;
			int currentRow = 0;
			int currentColumn = 0;
			while ((line = reader.readLine()) != null) {
				String[] elements = line.split(" ");
				for (int i = 0; i < elements.length; i++) {
					
					
					intensities[currentRow][currentColumn] = Integer.parseInt(elements[i]);
					// currentRow i currentColumn są na początku równe zero.
					// Należy je odpowiednio zwiększać, pamiętając o tym, żeby
					// nie wyjść poza zakres tablicy. Plik pgm może mieć w
					// wierszu dowolną ilość liczb, niekoniecznie równą liczbie
					// kolumn.
					// TODO Wasz kod
					
					currentColumn++;
					if(currentColumn==columns)
					{
						currentColumn=0;
						currentRow++;
					}
					
					if(currentRow==rows)
						break;
					
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
        		
		return intensities;
	}
	/**
	 * Metoda czyta plik pgm i zwraca tablicę odcieni szarości.
	 * @param fileName nazwa pliku pgm
	 * @return tablica odcieni szarości odczytanych z pliku
	 * @throws URISyntaxException jeżeli plik nie istnieje
	 */
	public int[][] readPgmFile(String fileName) throws URISyntaxException {
		int columns = 0;
		int rows = 0;
		int[][] intensities = null;

		Path path = Paths.get(fileName);

		//Path path = this.getPathToFile(fileName);
		
			 		
		try (BufferedReader reader = Files.newBufferedReader(path)) {
			// w kolejnych liniach kodu można / należy skorzystać z metody
			// reader.readLine()
			
			// pierwsza linijka pliku pgm powinna zawierać P2
			// TODO : Wasz kod
			if(!reader.readLine().equals("P2"))
			{
				throw new IOException("Zły format pliku.");
			}
			
				
			
			// druga linijka pliku pgm powinna zawierać komentarz rozpoczynający
			// się od #
			// TODO : Wasz kod
			
			if(!reader.readLine().startsWith("#"))
			{
				throw new IOException("Zły format pliku.");
			}
			
			// trzecia linijka pliku pgm powinna zawierać dwie liczby - liczbę
			// kolumn i liczbę wierszy (w tej kolejności). Te wartości należy
			// przypisać do zmiennych columns i rows.
			// TODO : Wasz kod
			
			String readLine = reader.readLine();
			String[] splitLine = readLine.split("\\s+");
			
			columns = Integer.parseInt(splitLine[0]);
			rows = Integer.parseInt(splitLine[1]);
			
			width=columns;
			height=rows;
			
			
			// czwarta linijka pliku pgm powinna zawierać 255 - najwyższą
			// wartość odcienia szarości w pliku
			// TODO : Wasz kod
			
			if(!reader.readLine().equals("255"))
			{
				throw new IOException("Zły format pliku.");
			}
			

			// inicjalizacja tablicy na odcienie szarości
			intensities = new int[rows][];

			for (int i = 0; i < rows; i++) {
				intensities[i] = new int[columns];
			}

			// kolejne linijki pliku pgm zawierają odcienie szarości kolejnych
			// pikseli rozdzielone spacjami
			String line = null;
			int currentRow = 0;
			int currentColumn = 0;
			while ((line = reader.readLine()) != null) {
				String[] elements = line.split(" ");
				for (int i = 0; i < elements.length; i++) {
					
					
					intensities[currentRow][currentColumn] = Integer.parseInt(elements[i]);
					// currentRow i currentColumn są na początku równe zero.
					// Należy je odpowiednio zwiększać, pamiętając o tym, żeby
					// nie wyjść poza zakres tablicy. Plik pgm może mieć w
					// wierszu dowolną ilość liczb, niekoniecznie równą liczbie
					// kolumn.
					// TODO Wasz kod
					
					currentColumn++;
					if(currentColumn==columns)
					{
						currentColumn=0;
						currentRow++;
					}
					
					if(currentRow==rows)
						break;
					
				}
			}
		} catch (Exception e) {
			//e.printStackTrace();
			throw new NullPointerException();
		}
		return intensities;
	}
	
	private Path getPathToFile(String fileName) throws URISyntaxException {
		URI uri = ClassLoader.getSystemResource(fileName).toURI();
		return Paths.get(uri);
	}
	
}
