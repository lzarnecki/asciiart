package pl.edu.pwr.pp;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferedImage;

import javax.swing.JComponent;

public class ImageComponent extends JComponent {

	private static final long serialVersionUID = 721898721501536143L;
	private BufferedImage image;

	public void setImage(BufferedImage img) {
		this.image = img;
	}

	@Override
	protected void paintComponent(Graphics g) {

		super.paintComponent(g);
		if (this.image != null) {
			g.drawImage(this.image, 0, 0, 300, 350, null);
		}
	}

	@Override
	public Dimension getMinimumSize() {
		return new Dimension(800, 600);
	}

	@Override
	public Dimension getPreferredSize() {
		return new Dimension(800, 600);
	}

}
