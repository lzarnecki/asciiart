package pl.edu.pwr.pp;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.JLabel;
import java.awt.event.ActionListener;
import java.io.File;
import java.awt.event.ActionEvent;

public class Dialog extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8451221391796523896L;
	private final JPanel contentPanel = new JPanel();
	private JTextField UrlAdressTextField;
	private JTextField filePathTextBox;

	private File file = null;
	private String imagePath = null;
	private Boolean isFile = false;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			Dialog dialog = new Dialog();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	String getImagePath() {
		return imagePath;
	}

	File getFile() {
		return file;
	}

	Boolean IsFile() {
		return isFile;
	}

	/**
	 * Create the dialog.
	 */
	public Dialog() {

		ButtonGroup group = new ButtonGroup();
		final JFileChooser fileChooser = new JFileChooser();

		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);

		filePathTextBox = new JTextField();
		filePathTextBox.setEditable(false);
		filePathTextBox.setBounds(45, 105, 227, 20);
		contentPanel.add(filePathTextBox);
		filePathTextBox.setColumns(10);

		UrlAdressTextField = new JTextField();
		UrlAdressTextField.setBounds(45, 165, 227, 20);
		contentPanel.add(UrlAdressTextField);
		UrlAdressTextField.setColumns(10);

		JRadioButton rdbtnZAdresuUrl = new JRadioButton("Z adresu URL");
		rdbtnZAdresuUrl.setBounds(45, 135, 132, 23);
		contentPanel.add(rdbtnZAdresuUrl);
		group.add(rdbtnZAdresuUrl);

		JRadioButton rdbtnZDysku = new JRadioButton("Z dysku");
		rdbtnZDysku.setSelected(true);
		rdbtnZDysku.setBounds(45, 75, 132, 23);
		group.add(rdbtnZDysku);
		contentPanel.add(rdbtnZDysku);

		JLabel lblWczytajPlik = new JLabel("  Wczytaj plik...");
		lblWczytajPlik.setBounds(45, 32, 158, 36);
		contentPanel.add(lblWczytajPlik);

		JButton btnWybierzPlik = new JButton("Wybierz plik");
		btnWybierzPlik.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				if (e.getSource() == btnWybierzPlik) {

					FileNameExtensionFilter filter = new FileNameExtensionFilter("Images", "jpg", "gif", "ppm", "png",
							"bmp");
					fileChooser.setFileFilter(filter);
					int returnVal = fileChooser.showOpenDialog(Dialog.this);

					if (returnVal == JFileChooser.APPROVE_OPTION) {
						file = fileChooser.getSelectedFile();
						filePathTextBox.setText(file.getPath());
					}
				}

			}
		});

		btnWybierzPlik.setBounds(183, 75, 89, 23);
		contentPanel.add(btnWybierzPlik);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						// OK Button
						if (rdbtnZDysku.isSelected()) {
							if (file != null) {
								imagePath = file.getPath();
								isFile = true;
							}
						}

						if (rdbtnZAdresuUrl.isSelected()) {
							try {
								imagePath = UrlAdressTextField.getText();
							} catch (Exception ee) {
								ee.printStackTrace();
							}

							isFile = false;
						}

						setVisible(false);
						dispose();
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						// Cancel button - close dialog
						imagePath = null;
						file = null;
						setVisible(false);
						dispose();
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}

	}
}
