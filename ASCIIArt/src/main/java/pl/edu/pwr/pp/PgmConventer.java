package pl.edu.pwr.pp;

import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;
import java.net.MalformedURLException;
import java.net.URISyntaxException;

public class PgmConventer 
{
	private ImageFileReader reader = new ImageFileReader();
	public BufferedImage image;

	public BufferedImage convert(String filePath) {
			int[][] data = null;

			try
			{
				data = reader.readPgmFile(filePath);
			}
			catch(URISyntaxException e)
			{
				e.printStackTrace();
			}

		
		int width = reader.getWidth();
		int height = reader.getHeight();

		BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_BYTE_GRAY);
		WritableRaster raster = image.getRaster();
		for (int y = 0; y < height; y++) {
			for (int x = 0; (x < width); x++) {
				raster.setSample(x, y, 0, data[y][x]);
			}
		}
		return image;
	}

	public BufferedImage convertFromUrl(String url) {
		int[][] data = null;
		
		try {
		data = reader.readPgmFileURL(url);
		} catch (MalformedURLException el) {
			el.printStackTrace();
		}
		
		int width = reader.getWidth();
		int height = reader.getHeight();

		BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_BYTE_GRAY);
		WritableRaster raster = image.getRaster();
		for (int y = 0; y < height; y++) {
			for (int x = 0; (x < width); x++) {
				raster.setSample(x, y, 0, data[y][x]);
			}
		}
		return image;
	}
}
