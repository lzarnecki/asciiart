package pl.edu.pwr.pp;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Graphics2D;
import java.awt.RenderingHints;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.imageio.ImageIO;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.awt.event.ActionEvent;
import javax.swing.LayoutStyle.ComponentPlacement;

public class MainWindow extends JFrame {

	private static final long serialVersionUID = -3529692836914166449L;
	private JPanel contentPane;

	private ImageComponent imageComponent = new ImageComponent();
	private String filePath = null;

	private BufferedImage img = null;
	private String imagePath = null;
	private Boolean IsFile = null;
	private Boolean IsPgm = null;

	private Integer quality = 10;
	private int[][] intensities = null;
	private Integer scale = 100;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainWindow frame = new MainWindow();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	/**
	 * Create the frame.
	 */
	public MainWindow() {
		JButton btnZapiszDoPliku;
		btnZapiszDoPliku = new JButton("Zapisz do pliku");
		btnZapiszDoPliku.setEnabled(false);

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 479, 454);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);

		JButton btnWczytajObraz = new JButton("Wczytaj obraz");
		btnWczytajObraz.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				Dialog dialog = new Dialog();
				dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
				dialog.setVisible(true);

				dialog.addWindowListener(new WindowAdapter() {
					@Override
					public void windowClosed(WindowEvent e) {

						imagePath = dialog.getImagePath();

						// Powrót jeśli brak sciezki
						if (imagePath == null) {
							btnZapiszDoPliku.setEnabled(false);
							return;
						}

						if (dialog.IsFile()) {
							IsFile = true;
							if (imagePath.contains(".pgm")) {
								IsPgm = true;
								btnZapiszDoPliku.setEnabled(true);
								PgmConventer conventer = new PgmConventer();

								img = conventer.convert(imagePath);

								
							} else {
								IsPgm = false;
								btnZapiszDoPliku.setEnabled(true);
								File file = new File(imagePath);
								try {
									img = ImageIO.read(file);
								} catch (IOException e1) {
									e1.printStackTrace();
								}
							}

						}

						if (!dialog.IsFile()) {
							IsFile = false;
							if (imagePath.contains(".pgm")) {
								IsPgm = true;
								btnZapiszDoPliku.setEnabled(true);
								PgmConventer conv = new PgmConventer();
								img = conv.convertFromUrl(imagePath);
							} else {
								try {
									IsPgm = false;
									URL url = new URL(imagePath);
									btnZapiszDoPliku.setEnabled(true);
									img = ImageIO.read(url);
								} catch (IOException e1) {
									e1.printStackTrace();
								}
							}

						}

						imageComponent.setImage(img);
						imageComponent.repaint();


						// If color
						if (img.getType() != BufferedImage.TYPE_BYTE_GRAY) {
							
							GrayConventer gconv = new GrayConventer();
							intensities = gconv.convert(img);
						}
					}
				});
			}
		});

		// ZAPIS
		btnZapiszDoPliku.addActionListener(e -> {
			SaveDialog saveDialog = new SaveDialog();
			saveDialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			saveDialog.setVisible(true);

			saveDialog.addWindowListener(new WindowAdapter() {
				@Override
				public void windowClosed(WindowEvent e) {

					filePath = saveDialog.getFilePath();

					ImageFileReader imageFileReader = new ImageFileReader();
					ImageFileWriter imageFileWriter = new ImageFileWriter();

					try {

						if (IsPgm) {
							if (IsFile)
								intensities = imageFileReader.readPgmFile(imagePath);
							else
								intensities = imageFileReader.readPgmFileURL(imagePath);
						}

						char[][] ascii = ImageConverter.intensitiesToAscii(intensities, quality);

						imageFileWriter.saveToTxtFile(ascii, filePath);

					} catch (Exception ee) {
						ee.printStackTrace();
					}

				}
			});
		} );

		JButton btnQuality = new JButton("Jakość");
		
		btnQuality.addActionListener(e -> {
			QualityDialog qualityDialog = new QualityDialog();
			qualityDialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			qualityDialog.setVisible(true);

			qualityDialog.addWindowListener(new WindowAdapter() {
				@Override
				public void windowClosed(WindowEvent e) {
					quality = qualityDialog.getQuality();
				}
			});
		} );
		
		JButton btnScale = new JButton("Skalowanie");
		btnScale.addActionListener(e -> {
			ScaleDialog scaleDialog = new ScaleDialog();
			scaleDialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			scaleDialog.setVisible(true);

			scaleDialog.addWindowListener(new WindowAdapter() {
				@Override
				public void windowClosed(WindowEvent e) {
					scale = scaleDialog.getScale();
					
					System.out.print(scale);
					
					if(scale != 100 && img != null)
					{
						int w = img.getWidth()*scale/100;
						int h = img.getHeight()*scale/100;
						
					    BufferedImage resizedImg = new BufferedImage(w, h, BufferedImage.TRANSLUCENT);
					    Graphics2D g2 = resizedImg.createGraphics();
					    g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
					    g2.drawImage(img, 0, 0, w, h, null);
					    g2.dispose();
					    
					    img = resizedImg;
					    intensities = new int[w][];

						for (int i = 0; i < w; i++) {
							intensities[i] = new int[h];
						}

						for (int y = 0; y < h; y++) {
							for (int x = 0; x < w; x++) {

								Color color = new Color(img.getRGB(x, y));
								intensities[x][y] = (int) (0.2989 * color.getRed() + 0.587 * color.getGreen()
										+ 0.114 * color.getBlue());
							}
						}
					}
				}
			});
		});

		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addComponent(btnScale, GroupLayout.DEFAULT_SIZE, 101, Short.MAX_VALUE)
						.addComponent(btnQuality, GroupLayout.DEFAULT_SIZE, 101, Short.MAX_VALUE)
						.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING, false)
							.addComponent(btnWczytajObraz, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
							.addComponent(btnZapiszDoPliku, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
					.addGap(29)
					.addComponent(imageComponent, 0, 303, Short.MAX_VALUE)
					.addGap(10))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(50)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(btnWczytajObraz)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(btnZapiszDoPliku)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(btnQuality)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(btnScale))
						.addComponent(imageComponent, GroupLayout.PREFERRED_SIZE, 350, GroupLayout.PREFERRED_SIZE))
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		contentPane.setLayout(gl_contentPane);
	}
}
