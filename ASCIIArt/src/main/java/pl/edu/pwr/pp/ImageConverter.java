package pl.edu.pwr.pp;

public class ImageConverter {

	/**
	 * Znaki odpowiadające kolejnym poziomom odcieni szarości - od czarnego (0)
	 * do białego (255).
	 */
	private static String INTENSITY_2_ASCII_LONG = "$@B%8&WM#*oahkbdpqwmZO0QLCJUYXzcvunxrjft/\\|()1{}[]?-_+~<>i!lI;:,\"^`'. ";

	public static char intensityToAscii(int intensity, int steps) {

		double v = (intensity * steps - 1) / 255;
		Long L = Math.round(v);
		int index = Integer.valueOf((steps - 1) - L.intValue());

		return INTENSITY_2_ASCII_LONG.charAt(index);
	}

	/**
	 * Metoda zwraca znak odpowiadający danemu odcieniowi szarości. Odcienie
	 * szarości mogą przyjmować wartości z zakresu [0,255]. Zakres jest dzielony
	 * na równe przedziały, liczba przedziałów jest równa ilości znaków w
	 * {@value #INTENSITY_2_ASCII_LONG}. Zwracany znak jest znakiem dla przedziału,
	 * do którego należy zadany odcień szarości.
	 * 
	 * 
	 * @param intensity
	 *            odcień szarości w zakresie od 0 do 255
	 * @return znak odpowiadający zadanemu odcieniowi szarości
	 */
	public static char intensityToAscii(int intensity) {
		// TODO Wasz kod
		if (intensity <= 25)
			return '@';

		if (intensity <= 51)
			return '%';

		if (intensity <= 76)
			return '#';

		if (intensity <= 102)
			return '*';

		if (intensity <= 127)
			return '+';

		if (intensity <= 153)
			return '=';

		if (intensity <= 179)
			return '-';

		if (intensity <= 204)
			return ':';

		if (intensity <= 230)
			return '.';

		if (intensity <= 255)
			return ' ';

		return ' ';
	}

	/**
	 * Metoda zwraca dwuwymiarową tablicę znaków ASCII mając dwuwymiarową
	 * tablicę odcieni szarości. Metoda iteruje po elementach tablicy odcieni
	 * szarości i dla każdego elementu wywołuje {@ref #intensityToAscii(int)}.
	 * 
	 * @param intensities
	 *            tablica odcieni szarości obrazu
	 * @return tablica znaków ASCII
	 */
	public static char[][] intensitiesToAscii(int[][] intensities, int steps) {
		// TODO Wasz kod

		int sizeRow = intensities.length;
		int sizeCol = intensities[0].length;

		char[][] result = new char[sizeRow][sizeCol];

		for (int i = 0; i < intensities.length; i++)
			for (int j = 0; j < intensities[i].length; j++)
				result[i][j] = intensityToAscii(intensities[i][j], steps);

		return result;
	}

}
